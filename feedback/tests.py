from django.test import TestCase, Client
from django.urls import resolve
from .views import feedbackViews
from .models import feedbackDatabase
from .forms import feedback

# Create your tests here.
class TestingUrls(TestCase):

    def test_apakah_url_yang_diakses_ada(self):
        response = Client().get("/feedback/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mengakses_function_yang_benar(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, feedbackViews)

class TestingModels(TestCase):

    def test_apakah_bisa_membuat_object(self):
        objectBaru = feedbackDatabase.objects.create(nama = "TESTING")
        objectBaru2 = feedbackDatabase.objects.create(cerita = "TESTING")
        jumlahObjectYangAda = feedbackDatabase.objects.all().count()
        self.assertEqual(jumlahObjectYangAda, 2)

class TestingViews(TestCase):

    def test_apakah_bisa_nama_post(self):
        test = 'Unknown'
        response_post = Client().post("/feedback/", {"Name" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_nama_blank(self):
        form = feedback(data={"Name" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Name'], ['This field is required.'])

    def test_apakah_bisa_cerita_post(self):
        test = 'Unknown'
        response_post = Client().post("/feedback/", {"Your_Story" : test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_apakah_bisa_post_jika_cerita_blank(self):
        form = feedback(data={"Your_Story" : ""})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['Your_Story'], ['This field is required.'])

    def test_apakah_nama_muncul_di_layar(self):
        response = Client().post('/feedback', {'Name': "al", 'Your_Story': "keren"})
        self.assertIn('al', response.content.decode())

    def test_apakah_cerita_muncul_di_layar(self):
        response = Client().post('/feedback', {'Name': "al", 'Your_Story': "keren"})
        self.assertIn('keren', response.content.decode()) 