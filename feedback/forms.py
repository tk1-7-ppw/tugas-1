from django import forms

class feedback(forms.Form):
    Name = forms.CharField(widget = forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Let us know your name!',
        'type' : 'text',
        'required' : True
    }))
    Your_Story = forms.CharField(widget = forms.Textarea(attrs={
        'class' : 'form-control',
        'placeholder' : 'Please tell us your story right here!',
        'type' : 'text',
        'required' : True
    }))