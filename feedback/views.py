from django.shortcuts import render, redirect
from . import forms, models

def feedbackViews(request):
    if(request.method == "POST"):
        tmp = forms.feedback(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.feedbackDatabase()
            tmp2.nama = tmp.cleaned_data["Name"]
            tmp2.cerita = tmp.cleaned_data["Your_Story"]
            tmp2.save()
        return redirect("/feedback")
    else:
        tmp = forms.feedback()
        tmp2 = models.feedbackDatabase.objects.all()
        tmp_dictio = {
            'form' : tmp,
            'feedback' : tmp2
        }
        return render(request, 'feedback/feedback.html', tmp_dictio)