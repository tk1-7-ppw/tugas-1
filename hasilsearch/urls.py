from django.urls import path
from .views import searchViews

urlpatterns = [
    path('', searchViews)
]