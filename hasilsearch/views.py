from django.shortcuts import render
from percountermakanan.models import databaseMakanan
from percounterminuman.models import databaseMinuman

# Create your views here.
def searchViews(request):
    value = request.GET.get('cari')
    if (value):
        response = {}
        hasilSearchMakanan = databaseMakanan.objects.all().filter(nama_makanan__icontains=value)
        hasilSearchMinuman = databaseMinuman.objects.all().filter(nama_minuman__icontains=value)
        response['makanan'] = hasilSearchMakanan
        response['minuman'] = hasilSearchMinuman
        return render(request, 'hasilsearch/hasilsearch.html', response)