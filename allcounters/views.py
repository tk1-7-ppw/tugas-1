from django.shortcuts import render

# Create your views here.
def food(request):
    return render(request, "allcounters/counter_food.html")

def drink(request):
    return render(request, "allcounters/counter_drink.html")