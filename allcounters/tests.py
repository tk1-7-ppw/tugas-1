from django.test import TestCase, Client
from django.urls import resolve
from .views import *
# Create your tests here.
class TestingUrls(TestCase):

    def test_apakah_url_makanan_yang_diakses_ada(self):
        response = Client().get("/counters/food/")
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_minuman_yang_diakses_ada(self):
        response = Client().get("/counters/drink/")
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_mengakses_makanan_function_yang_benar(self):
        found = resolve('/counters/food/')
        self.assertEqual(found.func, food)

    def test_apakah_mengakses_minuman_function_yang_benar(self):
        found = resolve('/counters/drink/')
        self.assertEqual(found.func, drink)