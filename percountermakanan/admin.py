from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(counterDatabaseBatagorSiomay)
admin.site.register(counterDatabaseGorengan)
admin.site.register(counterDatabaseIsabella)
admin.site.register(counterDatabaseGadoGado)
admin.site.register(counterDatabaseSoto)
admin.site.register(counterDatabaseMasJeb)
admin.site.register(counterDatabaseMasYono)
admin.site.register(counterDatabaseMieAyamBaso)
admin.site.register(counterDatabaseNasiGoreng)
admin.site.register(counterDatabaseNasiKuning)
admin.site.register(counterDatabaseNasiPecelAyam)
admin.site.register(counterDatabaseNasiPecelAyam2)
admin.site.register(counterDatabaseSate)
admin.site.register(counterDatabaseSotoBogor)
admin.site.register(counterDatabaseWarungKelontong)
admin.site.register(counterDatabaseDreamWaffle)
admin.site.register(counterDatabaseYoshinoya)
admin.site.register(databaseMakanan)