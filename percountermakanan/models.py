from django.db import models

# Create your models here.
class counterDatabaseBatagorSiomay(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseGorengan(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseIsabella(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseGadoGado(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseSoto(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseMasJeb(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseMasYono(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseMieAyamBaso(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseNasiGoreng(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseNasiKuning(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseNasiPecelAyam(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseNasiPecelAyam2(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseSate(models.Model) :
    commentM = models.TextField(blank=False)
    
class counterDatabaseSotoBogor(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseWarungKelontong(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseDreamWaffle(models.Model) :
    commentM = models.TextField(blank=False)

class counterDatabaseYoshinoya(models.Model) :
    commentM = models.TextField(blank=False)

class databaseMakanan(models.Model) :
    nama_makanan = models.CharField(blank=False, max_length=100)
    terdapat_di_counter = models.CharField(blank=False, max_length=100)
    deskripsi = models.TextField(blank=False)
    harga = models.DecimalField(blank=False, max_digits=9, decimal_places=3)