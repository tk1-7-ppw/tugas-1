from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(counterDatabaseMasYono)
admin.site.register(counterDatabaseMinumBoss)
admin.site.register(counterDatabaseEsKelapa)
admin.site.register(counterDatabaseMinumanKemasan)
admin.site.register(counterDatabaseMinumanSachet)
admin.site.register(counterDatabaseYoshinoya)
admin.site.register(databaseMinuman)