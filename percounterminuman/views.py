from django.shortcuts import render, redirect
from .forms import counterFeedback
from .models import *

# Create your views here.

def counterMasYono(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseMasYono()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/drink/masyono/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseMasYono.objects.all() 
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percounterminuman/counterMasYono.html', response)

def counterMinumBoss(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseMinumBoss()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/drink/minumboss/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseMinumBoss.objects.all() 
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percounterminuman/counterMinumBoss.html', response)

def counterEsKelapa(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseEsKelapa()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/drink/eskelapa/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseEsKelapa.objects.all() 
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percounterminuman/counterEsKelapa.html', response)

def counterMinumanKemasan(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseMinumanKemasan()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/drink/minumankemasan/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseMinumanKemasan.objects.all() 
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percounterminuman/counterMinumanKemasan.html', response)

def counterMinumanSachet(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseMinumanSachet()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/drink/minumansachet/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseMinumanSachet.objects.all() 
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percounterminuman/counterMinumanSachet.html', response)

def counterYoshinoya(request) :
    if (request.method == 'POST') :
        form = counterFeedback(request.POST)
        if (form.is_valid()) :
            newData = counterDatabaseYoshinoya()
            newData.commentM = form.cleaned_data['Let_us_know_your_opinion_about_this_counter']
            newData.save()
        return redirect('/counters/drink/yoshinoya/')
    else :
        form = counterFeedback()
        allComments = counterDatabaseYoshinoya.objects.all() 
        response = {
            'form' : form,
            'allComments' : allComments
        }
        return render(request, 'percounterminuman/counterYoshinoya.html', response)